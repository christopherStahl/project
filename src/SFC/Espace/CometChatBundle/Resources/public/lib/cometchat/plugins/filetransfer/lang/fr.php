<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$filetransfer_language[0] = 'Envoyer un fichier';
$filetransfer_language[1] = 'Quel fichier voudriez-vous envoyer ?';
$filetransfer_language[2] = 'Veuillez choisir un fichier en cliquant sur le bouton ci-dessous.';
$filetransfer_language[3] = '<b>AVERTISSEMENT:</b> N\'envoyez aucun contenu dont vous ne possédez pas les droits ou que l\'auteur ne vous a pas donné la permission d\'utiliser.';
$filetransfer_language[4] = 'Envoyer le fichier';
$filetransfer_language[5] = 'vous a envoyé un fichier';
$filetransfer_language[6] = 'Cliquez ici pour télécharger le fichier';
$filetransfer_language[7] = 'a envoyé un fichier avec succès';
$filetransfer_language[8] = 'Fichier envoyé avec succès. Fermeture de la fenêtre.';
$filetransfer_language[9] = 'a partagé un fichier';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////