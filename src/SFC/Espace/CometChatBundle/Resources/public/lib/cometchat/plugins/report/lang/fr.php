<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$report_language[0] = 'Signaler la Conversation';
$report_language[1] = 'Pour quelles raisons signalez-vous cet utilisateur ?';
$report_language[2] = 'Signaler l\'utilisateur';
$report_language[3] = 'Merci d\'avoir signalé cet utilisateur';
$report_language[4] = 'La fenêtre va bientôt se fermer';
$report_language[5] = 'Désolé, votre conversation avec cet utilisateur est vide.';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////