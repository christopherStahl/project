<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$block_language[0] = 'Bloquer l\'utilisateur';
$block_language[1] = 'Voulez-vous vraiment bloquer cet utilisateur ?';
$block_language[2] = 'L\'utilisateur a été bloqué avec succès.';
$block_language[3] = 'Gérer les utilisateurs bloqués';
$block_language[4] = 'Débloquer l\'utilisateur';
$block_language[5] = 'Gérer les utilisateurs bloqués';
$block_language[6] = 'Pas d\'utilisateur bloqué';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////