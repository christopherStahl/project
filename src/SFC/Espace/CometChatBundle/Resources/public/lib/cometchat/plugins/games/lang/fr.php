<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$games_language[0] = 'Jouer';
$games_language[1] = 'Veuillez choisir un jeu';
$games_language[2] = 'À quel jeu voudriez-vous jouer ?';
$games_language[3] = 'vous a envoyé une invitation à jouer.';
$games_language[4] = 'Cliquez ici pour l\'accepter';
$games_language[5] = ' ou ignorez tout simplement ce message.';
$games_language[6] = 'a envoyé une invitation à jouer avec succès.';
$games_language[7] = 'Invitation à jouer envoyée !';
$games_language[8] = 'À quel jeu voudriez-vous jouer ?';
$games_language[9] = 'Invitation à jouer envoyée à l\'utilisateur avec succès. Vous devriez recevoir une réponse rapidement. Fermeture de la fenêtre';
$games_language[10] = 'a accepté votre invitation à jouer.';
$games_language[11] = 'Cliquez ici pour ouvrir la fenêtre de jeu';
$games_language[12] = 'Jeu en cours !';
$games_language[13] = '<li id="256,912">8-Ball Pool</li>
<li id="1,735">Backgammon</li>
<li id="562,815">Battleships</li>
<li id="1607,815">Brilliant Turn</li>
<li id="557,815">Cheat</li>
<li id="6,735">Checkers</li>
<li id="2,735">Chess</li>
<li id="275,815">Conectomato</li>
<li id="86,881">Darts</li>
<li id="325,690">Domino</li>
<li id="4,735">Four in a Row</li>
<li id="64,815">Go</li>
<li id="273,665">GoldMiner</li>
<li id="567,825">Hex Empire</li>
<li id="326,815">Hexaru</li>
<li id="330,665">Kaban Tactics</li>
<li id="602,747">Knights Domain</li>
<li id="329,815">Mancala</li>
<li id="102,845">Marbles</li>
<li id="21,735">Match 4</li>
<li id="272,650">MineSweeper</li>
<li id="274,815">Ramble Scramble</li>
<li id="607,815">Russian Roulette</li>
<li id="15,735">SheepMe</li>
<li id="12,735">Sudoku</li>
<li id="327,845">Super Star Balls</li>
<li id="26,650">Tic Tac Toe</li>
';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////