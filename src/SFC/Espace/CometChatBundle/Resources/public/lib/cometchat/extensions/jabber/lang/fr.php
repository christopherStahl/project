<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$jabber_language[0] = 'Se connecter à Facebook/';
$jabber_language[1] = 'Avec qui voudriez-vous chatter ?';
$jabber_language[2] = 'Email :';
$jabber_language[3] = 'Mot de passe :';
$jabber_language[4] = 'Chat';
$jabber_language[5] = 'Facebook';
$jabber_language[6] = 'Connexion à';
$jabber_language[7] = 'Connexion en cours';
$jabber_language[8] = 'Se déconnecter de Facebook';
$jabber_language[9] = 'Détails de connexion erronés. Veuillez réessayer.';
$jabber_language[10] = 'Utilisateurs du site';
$jabber_language[11] = 'Amis Facebook';
$jabber_language[12] = 'Amis';
$jabber_language[13] = 'Se déconnecter de';
$jabber_language[14] = 'Il n\'y a pas d\'utilisateurs en ligne pour l\'instant.';
$jabber_language[15] = 'Chat';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////