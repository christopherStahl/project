<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$whiteboard_language[0] = 'Partager un tableau blanc';
$whiteboard_language[1] = 'Veuillez patienter au moins 10 secondes avant d\'essayer de partager à nouveau.';
$whiteboard_language[2] = 'a partagé son tableau blanc avec vous.';
$whiteboard_language[3] = 'Cliquez ici pour le voir.';
$whiteboard_language[4] = 'ou ignorez tout simplement ce message.';
$whiteboard_language[5] = 'a partagé avec succès son tableau blanc.';
$whiteboard_language[6] = 'regarde maintenant votre tableau blanc.';
$whiteboard_language[7] = 'a partagé un tableau blanc.';
$whiteboard_language[8] = 'Cliquez ici pour le voir.';
$whiteboard_language[9] = 'Tableau blanc';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////