<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$avchat_language[0] = 'Passer un appel audio/vidéo';
$avchat_language[1] = 'Veuillez patienter au moins 10 secondes avant d\'essayer de rappeler.';
$avchat_language[2] = 'vous a envoyé une demande de chat audio/vidéo.';
$avchat_language[3] = 'Cliquez ici pour l\'accepter';
$avchat_language[4] = 'ou ignorez tout simplement ce message.';
$avchat_language[5] = 'a envoyé une demande de chat audio/vidéo avec succès.';
$avchat_language[6] = 'a accepté votre demande de chat audio/vidéo.';
$avchat_language[7] = 'Cliquez ici pour ouvrir la fenêtre AV';
$avchat_language[8] = 'Chat Audio/Vidéo';
$avchat_language[9] = 'Vous avez des difficultés pour vous connecter ?';
$avchat_language[10] = 'There are several reasons why the audio/video chat may be stuck at initializing:
<br/><br/>
1. Audio/video chat requires P2P to establish connection. In order for it to work, your firewall must be configured to allow outgoing UDP traffic. While this is the case with most consumer or small office/home office (SOHO) firewalls, many corporate firewalls block UDP traffic altogether.
<br/><br/>
2. The user at the other end is behind a UDP blocking firewall and is unable to connect to you.
<br/><br/>
3. Our servers are facing issues and you are unable to connect.
';
$avchat_language[11] = 'La fenêtre va bientôt se fermer';
$avchat_language[12] = 'Users have been successfully invited. You can safely close this window.';
$avchat_language[13] = 'Utilisateurs invités avec succès !';
$avchat_language[14] = 'vous a invité à rejoindre une vidéoconférence. ';
$avchat_language[15] = 'Cliquez ici pour participer';
$avchat_language[16] = 'Veuillez choisir des utilisateurs';
$avchat_language[17] = 'Inviter des Utilisateurs';
$avchat_language[18] = 'Inviter des Utilisateurs';
$avchat_language[19] = 'a commencé une conversation vidéo.';
$avchat_language[20] = 'Cliquez ici pour participer à la conversation.';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////