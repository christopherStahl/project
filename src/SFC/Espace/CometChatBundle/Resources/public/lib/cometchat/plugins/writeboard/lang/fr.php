<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$writeboard_language[0] = 'Partager un document collaboratif';
$writeboard_language[1] = 'Veuillez patienter au moins 10 secondes avant d\'essayer de partager à nouveau.';
$writeboard_language[2] = 'a partagé un document.';
$writeboard_language[3] = 'Cliquez ici pour voir le document';
$writeboard_language[4] = 'ou ignorez tout simplement ce message.';
$writeboard_language[5] = 'a partagé un document avec succès.';
$writeboard_language[6] = 'regarde maintenant votre document.';
$writeboard_language[7] = 'Document collaboratif';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////