<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$chatrooms_language[100] = 'Salles de discussion';
$chatrooms_language[0] = 'Veuillez vous connecter pour utiliser nos salles de discussion.';
$chatrooms_language[1] = 'Veuillez choisir une salle de discussion publique/privée à laquelle vous voudriez vous inscrire.';
$chatrooms_language[2] = 'Créer une Salle de discussion';
$chatrooms_language[3] = 'Lobby';
$chatrooms_language[4] = '<a href="javascript:void(0);" onclick="javascript:leaveChatroom()">Quitter la salle</a> | <a href="javascript:void(0);" onclick="javascript:inviteUser()">Inviter un utilisateur</a>';
$chatrooms_language[5] = 'Les salles sur Invitation seulement n\'apparaîtront pas dans le lobby';
$chatrooms_language[6] = 'Moi';
$chatrooms_language[7] = ':  ';
$chatrooms_language[8] = 'Veuillez entrer le mot de passe de la salle de discussion';
$chatrooms_language[9] = 'Sortir';
$chatrooms_language[10] = 'Close popout';
$chatrooms_language[11] = 'A popout session is already in progress or another chatroom window is open. Please close the other windows to continue.';
$chatrooms_language[12] = 'Click here to retry';
$chatrooms_language[13] = ' a quitté la salle de discussion';
$chatrooms_language[14] = ' a rejoint la salle de discussion';
$chatrooms_language[15] = 'La fenêtre va bientôt se fermer';
$chatrooms_language[16] = 'Les utilisateurs ont été invités avec succès';
$chatrooms_language[17] = 'Utilisateurs Invités avec succès !';
$chatrooms_language[18] = 'vous a invité à rejoindre une salle de discussion. ';
$chatrooms_language[19] = 'Cliquez ici pour vous inscrire';
$chatrooms_language[20] = 'Inviter des Utilisateurs';
$chatrooms_language[21] = 'Veuillez choisir des utilisateurs';
$chatrooms_language[22] = 'Inviter des Utilisateurs';
$chatrooms_language[23] = 'Mot de passe erroné. Veuillez réessayer.';
$chatrooms_language[24] = '<img src="lock.png">';
$chatrooms_language[25] = '<img src="user.png">';
$chatrooms_language[26] = 'Veuillez saisir un mot de passe';
$chatrooms_language[27] = 'Nom';
$chatrooms_language[28] = 'Genre';
$chatrooms_language[29] = 'Salle publique';
$chatrooms_language[30] = 'Salle protégée par un mot de passe';
$chatrooms_language[31] = 'Salle sur Invitation seulement';
$chatrooms_language[32] = 'Mot de passe';
$chatrooms_language[33] = 'Créer une salle';
$chatrooms_language[34] = 'en ligne';
$chatrooms_language[35] = 'Salles de discussion';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////