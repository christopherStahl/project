<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* LANGUAGE */

$screenshare_language[0] = 'Partager mon écran';
$screenshare_language[1] = 'Veuillez patienter au moins 10 secondes avant d\'essayer de partager à nouveau.';
$screenshare_language[2] = 'a partagé son écran avec vous.';
$screenshare_language[3] = 'Cliquez ici pour voir son écran';
$screenshare_language[4] = 'ou ignorez tout simplement ce message.';
$screenshare_language[5] = 'a partagé avec succès son écran.';
$screenshare_language[6] = 'regarde maintenant votre écran.';
$screenshare_language[7] = 'Partage d\'écran';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////