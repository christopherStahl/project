<?php

namespace chris\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class chrisUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
