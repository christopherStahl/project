<?php
namespace chris\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
	public function accueilAction()
	{
		return $this->render('chrisUserBundle:User:accueil.html.twig');
	}
}