<?php
namespace chris\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use chris\NewsBundle\Entity\Article;
use chris\NewsBundle\Form\ArticleType;
use chris\NewsBundle\Form\ArticleEditType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request;


class NewsController extends Controller
{
	public function accueilAction(Request $request, $page)
	{

		if(
			$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ||
			$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')			
		) {
			# L'utilisateur est authentifié
			$_SESSION['useridCometChat']=$this->get('security.context')->getToken()->getUser()->getId();
		} else {
			echo "<h2>Anonyme</h2>";
		}
			
		if( $page < 1 )
		{
			throw $this->createNotFoundException('Page inexistante (page = '.$page.')');
		}
		$nombreParPage=3;
		$articles = $this->getDoctrine()
                     ->getManager()
                     ->getRepository('chrisNewsBundle:Article')
                     ->getArticles($nombreParPage, $page);
		return $this->render('chrisNewsBundle:News:accueil.html.twig',array('articles' => $articles,
																			'page'=> $page,
      																		'nombrePage' => ceil(count($articles)/$nombreParPage)
																			));
	}
	public function voirAction($slug)
	{
	  $em=$this->getDoctrine()->getManager();
	  $article=$em->getRepository('chrisNewsBundle:Article')
                                  ->findOneBy(array('slug'=>$slug));

	  $liste_articleCompetence = $em->getRepository('chrisNewsBundle:ArticleCompetence')
                                  ->findByArticle($article->getId());
	  
	  $allNews = $this->getDoctrine()->getManager()->getRepository('chrisNewsBundle:Article')->getAllArticles();
	  $nextNews = "";
	  $prevNews = "";
	  $i=0;
	  foreach ($allNews as $allN) {
	  	if ($slug === $allN["slug"]){
	  		if ($i !== 0) {
	  			$prevNews = $allNews[$i-1]["slug"];
	  		}
	  		if ($i !== count($allNews)-1) {
	  			$nextNews = $allNews[$i+1]["slug"];
	  		}
	  	}
	  	$i++;
	  
	  }
	  
	  return $this->render('chrisNewsBundle:News:voir.html.twig',array ('nextSlug' => $nextNews,'prevSlug' => $prevNews,'article' => $article,'liste_articleCompetence' => $liste_articleCompetence));
	}
	public function ajouterAction()
	{
		if (!$this->get('security.context')->isGranted('ROLE_AUTEUR')) {
			// Sinon on déclenche une exception « Accès interdit »
			throw new AccessDeniedHttpException('Accès limité aux auteurs');
		}
		// On crée un objet Article
		$article = new Article();
	
		// On crée le FormBuilder grâce à la méthode du contrôleur
		$form = $this->createForm(new ArticleType, $article);
		
	    // Reste de la méthode qu'on avait déjà écrit
	    if ($this->getRequest()->getMethod() == 'POST') {
	    	$form->bind($this->getRequest());
	    	
	    	// On vérifie que les valeurs entrées sont correctes
	    	// (Nous verrons la validation des objets en détail dans le prochain chapitre)
	    	if ($form->isValid()) {
	    		$article->setAuteur($this->get('security.context')->getToken()->getUser());
	    		
	    		// On l'enregistre notre objet $article dans la base de données
	    		$em = $this->getDoctrine()->getManager();
	    		$em->persist($article);
	    		$em->flush();
		      $this->get('session')->getFlashBag()->add('info', 'Article bien enregistré');
		      return $this->redirect( $this->generateUrl('chrisNewsVoir', array('slug' => $article->getSlug() )) );
	    	}
	    }
	    
		return $this->render('chrisNewsBundle:News:ajouter.html.twig',array(
					'form' => $form->createView(),
			));
	}
	public function modifierAction(Article $article)
	{
		if (!$this->get('security.context')->isGranted('ROLE_AUTEUR')) {
			// Sinon on déclenche une exception « Accès interdit »
			throw new AccessDeniedHttpException('Accès limité aux auteurs');
		}
		// On crée le FormBuilder grâce à la méthode du contrôleur
		$article->setDateEdition(new \Datetime());
		$form = $this->createForm(new ArticleEditType(), $article);
		if ($this->getRequest()->getMethod() == 'POST') {
			$form->bind($this->getRequest());
		
			// On vérifie que les valeurs entrées sont correctes
			// (Nous verrons la validation des objets en détail dans le prochain chapitre)
			if ($form->isValid()) {
				// On l'enregistre notre objet $article dans la base de données
				$em = $this->getDoctrine()->getManager();
				$em->persist($article);
				$em->flush();
				$this->get('session')->getFlashBag()->add('info', 'Article bien enregistré');
				return $this->redirect( $this->generateUrl('chrisNewsVoir', array('slug' => $article->getSlug() )) );
			}
		}
		return $this->render('chrisNewsBundle:News:modifier.html.twig',array('article' => $article,'form' => $form->createView()));
	}
	
	public function supprimerAction(Article $article)
	{
		if (!$this->get('security.context')->isGranted('ROLE_AUTEUR')) {
			// Sinon on déclenche une exception « Accès interdit »
			throw new AccessDeniedHttpException('Accès limité aux auteurs');
		}
	   // On crée un formulaire vide, qui ne contiendra que le champ CSRF
	    // Cela permet de protéger la suppression d'article contre cette faille
	    $form = $this->createFormBuilder()->getForm();
	
	    $request = $this->getRequest();
	    if ($request->getMethod() == 'POST') {
	      $form->bind($request);
	
	      if ($form->isValid()) {
	        // On supprime l'article
	        $em = $this->getDoctrine()->getManager();
	        $em->remove($article);
	        $em->flush();
	
	        // On définit un message flash
	        $this->get('session')->getFlashBag()->add('info', 'Article bien supprimé');
	
	        // Puis on redirige vers l'accueil
	        return $this->redirect($this->generateUrl('chrisNewsAccueil'));
	      }
	    }
	
	    // Si la requête est en GET, on affiche une page de confirmation avant de supprimer
	    return $this->render('chrisNewsBundle:News:supprimer.html.twig', array(
	      'article' => $article,
	      'form'    => $form->createView()
	    ));
	}
	public function menuAction($nombre)
	{
		// On fixe en dur une liste ici, bien entendu par la suite on la récupérera depuis la BDD !
		 $liste = $this->getDoctrine()
                  ->getManager()
                  ->getRepository('chrisNewsBundle:Article')
                  ->findBy(
                    array(),          // Pas de critère
                    array('date' => 'desc'), // On trie par date décroissante
                    $nombre,         // On sélectionne $nombre articles
                    0                // À partir du premier
                  );
	
		return $this->render('chrisNewsBundle:News:menu.html.twig', array(
				'liste_articles' => $liste // C'est ici tout l'intérêt : le contrôleur passe les variables nécessaires au template !
		));
	}
}