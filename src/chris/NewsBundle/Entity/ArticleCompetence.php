<?php

namespace chris\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticleCompetence
 */
class ArticleCompetence
{
    /**
     * @var string
     */
    private $niveau;

    /**
     * @var \chris\NewsBundle\Entity\Article
     */
    private $article;

    /**
     * @var \chris\NewsBundle\Entity\Competence
     */
    private $competence;


    /**
     * Set niveau
     *
     * @param string $niveau
     * @return ArticleCompetence
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return string 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set article
     *
     * @param \chris\NewsBundle\Entity\Article $article
     * @return ArticleCompetence
     */
    public function setArticle(\chris\NewsBundle\Entity\Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \chris\NewsBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set competence
     *
     * @param \chris\NewsBundle\Entity\Competence $competence
     * @return ArticleCompetence
     */
    public function setCompetence(\chris\NewsBundle\Entity\Competence $competence)
    {
        $this->competence = $competence;

        return $this;
    }

    /**
     * Get competence
     *
     * @return \chris\NewsBundle\Entity\Competence 
     */
    public function getCompetence()
    {
        return $this->competence;
    }
}
