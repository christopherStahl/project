<?php

namespace chris\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Article
 */
class Article
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $titre;

    /**
     * @var string
     */
    private $auteur;

    /**
     * @var string
     */
    private $contenu;
    
    /**
     * @var \chris\NewsBundle\Entity\Image
     */
    private $image;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categories;
    
    /**
     * @var boolean
     */
    private $publication;
    
    /**
     * @var \DateTime
     */
    private $dateEdition;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commentaires;
    
    /**
     * @var integer
     */
    private $nbCommentaires;
    
    /**
     * @var string
     */
    private $slug;
    
    
    public function __construct()
    {
    	$this->date = new \Datetime();
    	$this->dateEdition = new \Datetime();
    	$this->publication = true;
    	$this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->commentaires = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->nbCommentaires = 0;
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Article
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     * @return Article
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Article
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }


    /**
     * Set publication
     *
     * @param boolean $publication
     * @return Article
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return boolean 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set image
     *
     * @param \chris\NewsBundle\Entity\Image $image
     * @return Article
     */
    public function setImage(\chris\NewsBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \chris\NewsBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }



    /**
     * Add categories
     *
     * @param \chris\NewsBundle\Entity\Categorie $categories
     * @return Article
     */
    public function addCategory(\chris\NewsBundle\Entity\Categorie $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \chris\NewsBundle\Entity\Categorie $categories
     */
    public function removeCategory(\chris\NewsBundle\Entity\Categorie $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }


    /**
     * Add commentaires
     *
     * @param \chris\NewsBundle\Entity\Commentaire $commentaires
     * @return Article
     */
    public function addCommentaire(\chris\NewsBundle\Entity\Commentaire $commentaires)
    {
        $this->commentaires[] = $commentaires;
        $commentaires->setArticle($this);
        return $this;
    }

    /**
     * Remove commentaires
     *
     * @param \chris\NewsBundle\Entity\Commentaire $commentaires
     */
    public function removeCommentaire(\chris\NewsBundle\Entity\Commentaire $commentaires)
    {
        $this->commentaires->removeElement($commentaires);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }


    /**
     * Set dateEdition
     *
     * @param \DateTime $dateEdition
     * @return Article
     */
    public function setDateEdition($dateEdition)
    {
        $this->dateEdition = $dateEdition;

        return $this;
    }

    /**
     * Get dateEdition
     *
     * @return \DateTime 
     */
    public function getDateEdition()
    {
        return $this->dateEdition;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate()
    {
        $this->setDateEdition(new \Datetime());
    }




    /**
     * Set nbCommentaires
     *
     * @param integer $nbCommentaires
     * @return Article
     */
    public function setNbCommentaires($nbCommentaires)
    {
        $this->nbCommentaires = $nbCommentaires;

        return $this;
    }

    /**
     * Get nbCommentaires
     *
     * @return integer 
     */
    public function getNbCommentaires()
    {
        return $this->nbCommentaires;
    }

    


    /**
     * Set slug
     *
     * @param string $slug
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function contenuValide(ExecutionContextInterface $context)
    {
    	$mots_interdits = array('échec', 'abandon');
    
    	// On vérifie que le contenu ne contient pas l'un des mots
    	if (preg_match('#'.implode('|', $mots_interdits).'#', $this->getContenu())) {
    		// La règle est violée, on définit l'erreur et son message
    		// 1er argument : on dit quel attribut l'erreur concerne, ici « contenu »
    		// 2e argument : le message d'erreur
    		$context->addViolationAt('contenu', 'Contenu invalide car il contient un mot interdit.', array(), null);
    	}
    }
}
