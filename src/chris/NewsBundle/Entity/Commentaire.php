<?php

namespace chris\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 */
class Commentaire
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $auteur;

    /**
     * @var string
     */
    private $contenu;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \chris\NewsBundle\Entity\Article
     */
    private $article;

    public function __construct()
    {
    	$this->date = new \Datetime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     * @return Commentaire
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Commentaire
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Commentaire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * Set article
     *
     * @param \chris\NewsBundle\Entity\Article $article
     * @return Commentaire
     */
    public function setArticle(\chris\NewsBundle\Entity\Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \chris\NewsBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @ORM\PrePersist
     */
    public function increaseNbCom()
    {
        $nbCommentaires = $this->getArticle()->getNbCommentaires();
  		$this->getArticle()->setNbCommentaires($nbCommentaires+1);
    }

    /**
     * @ORM\PreRemove
     */
    public function descreaseNbCom()
    {
        $nbCommentaires = $this->getArticle()->getNbCommentaires();
    	$this->getArticle()->setNbCommentaires($nbCommentaires-1);
    }
}
